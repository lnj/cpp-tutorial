# Learning C++

Learning something new is the easiest when you can immidiately make use of the new knowledge gained, so in this guide we'll try to finish the boring basics as quickly as possible, so you can continue learning in a project that you care about.

This will be easier to complete if you already know about concepts of programming, but it should be possible to work through it otherwise by looking up concepts you don't know already.

# Basic Setup

In order to make learning a little easier, we'll make use of an integrated development environment that will already tell you about syntax errors and other issues while you are writing.
For this we'll use QtCreator, which you can download [here](https://www.qt.io/download-qt-installer) in combination with the rest of the Qt framework which we can maybe use later.

After QtCreator is installed, start it and click "New" to create a new project. Then select "Project without Qt", and then "pure C++ application". You can leave the other options to their default values.

Your project will start with something like the following:
```
#include <iostream>

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    return 0;
}
```

This code will just print "Hello World" to the console, which is not of much use to us right now, so you can remove everything between the two curly braces.

# Basic concepts
<!-- C++ is a language that combines aspects of object-oriented programming and functional programming. -->

## Variables
We'll start by creating a variable. A variable can store data of a specific type, like a number, a string of characters of more advanced types.
In C++, every variable needs to have a set type which can't be changed after the variable was initially created.

The basic syntax for a variable is
```C++
Type variable_name;
```
It is important to note that every statement ends with a semicolon.

There is a number of basic type from which all other advanced types are built.

The ones most interesting in the beginning are:
--------------------------------------------------------------
| Use                                    | type name in C++  |
|----------------------------------------|-------------------|
|An integer number                       | int               |
|A type allowing for incomplete numbers  | float             |
|Two different values, e.g true or false | bool              |
|                                        |                   |
--------------------------------------------------------------

A variable can be initialized to a value like this:
```
int number = 1;
```

It is not neccessary to specify the type every time you create a variable. If it is already clear which type a variable has, because for example you are initializing it to a value, alternatively you can let the compiler find out the type for you by using `auto` as a type. `auto` is not really a type, but a placeholder for a type that the compiler is yet to find out.

```
auto number = 1;
```

In this case, auto expands to int, as above. To make the code easier to understand later, it often makes sense to specify the type even if it already clear.

## Operators

C++ has a concept of operators, which allow doing specific things with variables. Basic types support a number of arithmethic operators, like `+`, `-`, `*` and so on.

If we re-use the line of code that we used in the begginning, we can do something with the number, like adding another number to it
```C++
int number = 1;
int twice_number = number * 2;
```

Some operators can also modify values in place, for example:

```C++
int number = 1;
number += 2;
```
changes the value of `number` to the value of `number + 2`.

## Constants

In many cases, you just need to store a value but there is no need change it later, for that reason you can mark variables as const.

```C++
const int seconds_of_a_minute = 60;
```

It is reasonable to assume this value is never going change, so let's make sure we don't mistakenly change it.

# More advanced types

Everything but the basic types is not available by default, so it is necessary to tell the compiler that you want to use them.

For example if you want to store a text, you can make use of the `string` type. The string is one of the types that are always available if there's a a C++ compiler. Those types are part of the so called standard library, called `std`.

You can include it into your current file as follows:
```C++
#include <string>
```

Then you can use it just like we did with the basic types in the beginning:
```C++
std::string hello_world = "Hello World";
```

# printing to the console

To make it easier to see what our program is doing, we can print variables to the console.

For that we'll use another part of the standard library, called `iostream`, where io stands for input / output. A stream is a type that supports the `<<` operator. Basically this type doesn't have a single value, but you can continously stuff more values into it.

```C++
int a = 1;
std::cout << "Content of a " << a;
```

# branches

As you can easily see, a continous flow of instructions isn't enough to make a program that can really do things of much use, we also need branches. A branch can decide between two different behaviours depending on a condition, often a bool (true or false).

The code that should be executed in the cases is delimited by a curly brace.

```C++
bool exists = true;

if (exists) {
    std::cout << "It exists";
} else {
    std::cout << "It doesn't exist"
}
```

# Loops

A loop executes the same code multiple times. The most basic loop is the while loop, which executes code while a condition is matched.

```C++
int counter = 0;
while (counter < 10) {
    counter++;
    std::cout << counter << std::endl;
}
```

This code counts up until it has counted to 10.

# Functions

It doesn't make sense to write code again if we need it later in the program, for that reason there are functions. They help to make code re-usable.

A function is a clearly defined part of code, which creates some new data from data we give it.

The following code needs to values, and returns a number (an integer). The number is the sum of both values.
```C++
int add(int value1, int value2) {
    return value1 + value2;
}
```

As you might have noticed, the main we started with in the beginning is also a function. We can call functions from other functions, just like we can call them from the main function, which is the entrypoint to our program.

The following code uses our add function and stores the result in a variable. The variable should now contain 3.
```C++
int sum = add(1, 2);
```

# vectors and range for

Often we need a sequence of values instead of a single one.

A vector is a generic type, which can contain more than one type.
The following code creates a vector that can contain numbers.

```C++
std::vector<int> temperatures = {21, 32, 30, 27, 29};
int sum = 0;
for (int temperature : temperatures) {
    sum += temperature;
}

std::cout << sum / temperatures.size();
```

It also demonstrates a new loop we didn't previously see, the for loop. The for loop can go through a sequence like a vector, and run code for each element in it.
In this case, we add the value to variable defined outside of the repeating code.

In the we print the result devided by the number of elements in the vector. `size()` is a function, but if you haven't used object-oriented programming before, you might wonder why it is executed in this way, apparently belonging to the vector. We'll look into that as soon as possible, but first we need to have a closer look at the vector.
It turns out that the vector is a class.

# Classes

Classes are a kind of blueprints grouping some data and functionality together. They can be used to define types like the `std::vector` we just used.

Classes have members, which are basically just variables belonging to them. They are used to store the internal data of a class.

The following class demonstrates the combination of member variables (class variables) and functions.

```C++
class Tree {
public:
    int age = 0;
    int height = 0;
    std::vector<int> leaves = {};

    void drop_leaves() {
        leaves.clear();
    };
};
```

Defining a class alone doesn't do anything yet when we launch the program, it is just a blueprint after all.
To create a specific instance, an object, we have to use a variable again.
```C++
Tree tree_in_front_of_my_house;
tree_in_front_of_my_house.age += 1;
tree_in_front_of_my_house = {1, 2, 3, 4};
```

To make it clearer, it is good style to prefix all members with `m_`. This also prevents name clashes if a function with the same name exists.

```C++
class Tree {
public:
    int m_age;
    int m_height;
    std::vector<Leave> m_leaves;
    
    void drop_leaves() {
        leaves.clear();
    };
};
```

